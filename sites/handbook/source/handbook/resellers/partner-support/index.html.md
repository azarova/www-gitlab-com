---
layout: handbook-page-toc
title: "Partner Support"
---
 
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}




# Welcome to the Partner Support page
This page is intended for Sales and Channel team members to review quick links and common questions that come to the Partner Help Desk team.

## Meet the Team
### Who We Are
- Evon Collett (she/her) - Manager, Partner Help Desk
- Reena Yap (she/her) - Senior Partner Help Desk Specialist, APAC focus
- Kim Stagg (they/them) - Senior Partner Help Desk Specialist, AMER/US-PubSec focus
- Yulia Imbert (she/her) - Partner Help Desk Specialist, EMEA focus

## Team Responsibilities
- Channel account record support in SFDC
- Channel account data management in Impartner
- Day-to-day partner questions via partnersupport@gitlab.com
- Partner rebate payments and tracking
- Additional responsibilities to be documented

### Out of Scope
_As part of the programs team, the Partner Help Desk team is not a part of the sales process, but works with partners to move throughout the partner program and increase their partner benefits._
- Channel quoting & quoting assistance (please chatter `@partner operations` on the record)
   - Please note that `@partner operations` is responsible for distributor quoting. [The sales team is responsible for reseller-direct quoting.](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#opportunity-requirements-to-request-a-quote) It is not the Channel Manager's responsibility to quote channel opportunities.
- SFDC opportunity record assistance (please chatter `@partner operations` on the record)
- Pre- and post-sales support. The SA, sales, and support teams should be utilized for these needs. 
- Billing account set-up (please chatter the partner account owner or `@billing ops`)
- Channel Account Management and training
   - The Partner Help Desk provides enablement tools, how-to guides, and support getting into GitLab’s partner systems. We answer questions and troubleshoot for partners and CAMs who may have questions via partnersupport@gitlab.com. One-on-one trainings and live portal walkthroughs are out of scope. For assistance with these, CAMs are encouraged to use the tools listed below.

_For a more detailed explanation of the differences between the Partner Help Desk and Partner Operations, visit [Tagging Partner Operations or Partner Help Desk in Salesforce](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#tagging-partner-operations-or-partner-help-desk-in-salesforce)._

## How to contact us 
The **#channel-programs-ops** Slack should be the first mode of contact for GitLab team members for all urgent and/or non-SFDC-record-related. If the request is related to an SFDC partner account record, please chatter `@partner help desk` on the record. 

Slack Best Practices  
**Please avoid contacting the Partner Help Desk team members directly via Slack.** Utlizing the #channel-programs-ops Slack channel is best to ensure timely coverage, helps others who may have similar questions, and aligns with our [Transparency value](https://about.gitlab.com/handbook/values/#transparency).

Partners can contact the Partner Support team at partnersupport@gitlab.com.

## Partner Support links & documents

<details>
<summary markdown="span">Quick Links</summary>
- [Partner Guide (internal-only)](https://docs.google.com/document/d/1HOzcdl22JRRbqo0SLPDsoUiM8NpNkf6-L2eiUHr6HZ4/edit)
- [Gitlab Partner Portal](https://partners.gitlab.com/English/)
- [PHD Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/4547475?label_name%5B%5D=Partner%20Help%20Desk)
- [Channels Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel)
- [Internal GitLab Channel Partner Program Discounts and Incentive Guide](https://docs.google.com/document/d/1qiT_2EsnL20c4w0hyZ_CGaJQIzj8CSCsHERoR80cwws/edit?usp=sharing)
- [Working with Partner Help Desk](https://docs.google.com/presentation/u/0/d/1tT5xcx04mlFyuftL5ECPH1VCZ0pkhW7caqnCkM7a-Ro/edit)

</details>

<details>
<summary markdown="span">Handbook Pages</summary>
- [Channel Partner Handbook](https://about.gitlab.com/handbook/resellers/)
- [Channel Operations](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops)
- [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#)
- [Quoting Channel Deals](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#quoting-channel-deals)
</details>

<details>
<summary markdown="span">Partner Portal Asset Links (portal login required)</summary>
_These links are useful to share with authorized reseller partners who already have access to the partner portal._
- [Partner Guide](https://partners.gitlab.com/prm/English/s/assets?id=404715)
- [Online Agreement](https://partners.gitlab.com/prm/English/s/assets?id=290599)
- [Deal Registration Guide](https://partners.gitlab.com/prm/English/s/assets?id=391183)
- [Partner Locator Guide](https://partners.gitlab.com/prm/English/s/assets?id=288270)
</details>

  
## Creating a Partner Portal Login
_This information can also be found in the Partner Guide [internal-only link](https://docs.google.com/document/d/1HOzcdl22JRRbqo0SLPDsoUiM8NpNkf6-L2eiUHr6HZ4/edit)_
If a partner organization already exists in our system (Impartner/SFDC), but a new rep needs a login, the instructions are fairly simple. That person should visit our [partner portal](partners.gitlab.com), and scroll down to the login section. On the left side of the page, there's an option to "Request Portal Access."   

![PHD_HB_Request_Portal_Access](/handbook/resellers/partner-support/source/images/PHD_HB_Request_Portal_Access.png) 

The partner will be directed to a page that asks them to confirm the company they work for. After they confirm, they should click next, and fill in their information. After submitting the information, they'll receive login credentials.


_More Information to Come!_  



